# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Viewing the profile results from Pyom
* Uses modified cProfilev (for python 3.x support)
* Modifications made to allow profile viewer to work

### How do I get set up? ###

* To perform a profile:


* Open a command line
* Navigate to pysrc directory
* python -m cProfile -o result_file pyom.py
* The game will boot up with the profiler
* Play the game/let it run
* When finished shut the game down
* Navigate to folder with cprofilev.py
* If not already, add result file, or have path to result file handy
* Run the profile viewer against the profile result file
* python cprofilev.py result_file
* Connect to http://localhost:4000
* Enjoy.

### Contribution guidelines ###

* The pyom project can be found at https://bitbucket.org/mudbytes/pyom
* Pyom is a total conversion of ROM in C to Python